package net.paoloambrosio.controller;

import net.paoloambrosio.service.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.Collections;

@Controller
public class SearchController {

    @Autowired
    private MessageRepository messageRepository;

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public ModelAndView userPage(@RequestParam(required = false) String query) {
        if (query != null) {
            return new ModelAndView("search", "messages", messageRepository.findByContent(query));
        } else {
            return new ModelAndView("search", "messages", Collections.EMPTY_LIST);
        }
    }

}
