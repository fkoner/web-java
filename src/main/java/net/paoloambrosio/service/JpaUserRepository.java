package net.paoloambrosio.service;

import net.paoloambrosio.model.User;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class JpaUserRepository implements UserRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public void save(final User user) {
        entityManager.persist(user);
    }

    @Override
    public List<User> findAll() {
        return entityManager.createQuery("SELECT o FROM User o",
                User.class).getResultList();
    }

    @Override
    public User findByUsername(String username) {
        return entityManager.find(User.class, username);
    }

    @Transactional
    public void deleteAll() {
        entityManager.createQuery("DELETE FROM User").executeUpdate();
    }

}
