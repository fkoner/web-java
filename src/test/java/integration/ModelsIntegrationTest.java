package integration;

import net.paoloambrosio.model.Message;
import net.paoloambrosio.model.User;
import net.paoloambrosio.service.MessageRepository;
import net.paoloambrosio.service.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static com.natpryce.makeiteasy.MakeItEasy.*;
import static org.junit.Assert.assertEquals;
import static utils.databuilder.MessageMaker.TestMessage;
import static utils.databuilder.MessageMaker.author;
import static utils.databuilder.UserMaker.TestUser;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:test-context.xml"})
@SuppressWarnings("unchecked")
public class ModelsIntegrationTest {

    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    private UserRepository userRepository;

    @Test
    public void somethingMeaningful() {
        User user = make(a(TestUser));
        userRepository.save(user);
        assertEquals(1, userRepository.findAll().size());

        Message message = make(a(TestMessage, with(author, user)));
        messageRepository.save(message);
        assertEquals(1, messageRepository.findAll().size());
    }

    @Test
    public void somethingNotSoMuch() {
        User user = make(a(TestUser));
        userRepository.save(user);

        messageRepository.save(make(a(TestMessage, with(author, user))));
        assertEquals(1, messageRepository.findByAuthor(user.getUsername()).size());

        messageRepository.save(make(a(TestMessage, with(author, user))));
        messageRepository.save(make(a(TestMessage, with(author, user))));
        assertEquals(3, messageRepository.findByAuthor(user.getUsername()).size());
    }

    @Test
    public void everythingCanBeDeleted() {
        messageRepository.deleteAll();
        userRepository.deleteAll();
    }
}
