package utils.page;

import net.paoloambrosio.model.User;
import org.springframework.stereotype.Component;

@Component
public class UserPage extends BrowserPage {

    public void visitFor(final User user) {
        webDriver().get(BASE_URL + "/users/" + user.getUsername());
    }
}
