package utils.databuilder;

import com.natpryce.makeiteasy.Instantiator;
import com.natpryce.makeiteasy.Property;
import com.natpryce.makeiteasy.PropertyLookup;
import net.paoloambrosio.model.User;

import java.util.UUID;

public class UserMaker {

    public static final Property<User, String> username = new Property<User, String>();

    public static final Instantiator<User> TestUser = new Instantiator<User>() {
        @Override
        public User instantiate(PropertyLookup<User> lookup) {
            User user = new User(lookup.valueOf(username, UUID.randomUUID().toString()));
            return user;
        }
    };

}
